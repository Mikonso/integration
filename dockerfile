# Użyj oficjalnego obrazu Pythona jako bazowego
FROM python:3.11-alpine3.19

RUN apk add --no-cache bash

# Ustaw katalog roboczy
WORKDIR /app

# Skopiuj pliki projektu do katalogu roboczego
COPY . /app

# Instaluj zależności z pliku requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

RUN apk add --no-cache bash

COPY wait-for-it.sh /app/wait-for-it.sh


# Ustaw port, na którym będzie działać serwer
EXPOSE 5000

COPY . /app/

# Nadaj uprawnienia do uruchamiania dla setup.sh i uruchom go
RUN chmod +x setup.sh
RUN ./setup.sh

RUN chmod +x /app/wait-for-it.sh

# Uruchom serwer Flask
CMD ["python", "main.py"]